<?php
$eventCategoryName = 'geen naam gevonden';
foreach ($model['listEventCategory'] as $item) {
    if ($item['Id'] == $model['row']['EventCategoryId']) {
        $eventCategoryName = $item['Name'];
    }
}

$eventTopicName = 'geen naam gevonden';
foreach ($model['listEventTopic'] as $item) {
    if ($item['Id'] == $model['row']['EventTopicId']) {
        $eventTopicName = $item['Name'];
    }
}
?>
<section class="show-room entity">
    <form id="form" method="" action="" class="detail">
        <nav class="command-panel">
            <h2 class="banner">EventCategorie</h2>
            <a href="/Event/UpdatingOne/{placeholder}" class="tile">
                <span class="icon-pencil"></span>
                <span class="screen-reader-text">Updating One</span>
            </a>
            <a href="/Event/InsertingOne" class="tile">
                <span class="icon-plus"></span>
                <span class="screen-reader-text">Inserting One</span>
            </a>
            <a href="/EventCategory/deleteOne/<?php echo $model['row']['Id'];?>" class="tile">
                <span class="icon-bin"></span>
                <span class="screen-reader-text">Delete One</span>
            </a>
            <a href="/Event/Index.php" class="tile">
                <span class="icon-cross"></span>
                <span class="screen-reader-text">Annuleren</span>
            </a>
        </nav>
        <fieldset>
            <div>
                <label for="Name">Naam</label>
                <div><?php echo $model['row']['Name']; ?></div>
                <div><?php echo $model['row']['Location']; ?></div>
                <div><?php echo $model['row']['Starts']; ?></div>
                <div><?php echo $model['row']['Ends']; ?></div>
                <div><?php echo $model['row']['Image']; ?></div>
                <div><?php echo $model['row']['Description']; ?></div>
                <div><?php echo $model['row']['OrganiserName']; ?></div>
                <div><?php echo $model['row']['OrganiserDescription']; ?></div>
                <div><?php echo $eventCategoryName; ?></div>
                <div><?php echo $eventTopicName ?></div>
            </div>
        </fieldset>
        <div class="feedback"></div>
    </form>
    <?php include('ReadingAll.php'); ?>
</section>