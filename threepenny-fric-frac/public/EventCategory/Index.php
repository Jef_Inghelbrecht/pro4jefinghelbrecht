<!DOCTYPE html>
<html lang="nl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fric-frac simple CRUD</title>
</head>

<body>
    <header>
        <nav class="control-panel">
            <a href="/index.php" class="tile">Admin Index</a>
        </nav>
        <h1 class="banne.control-panelr">Fric-frac</h1>
    </header>
    <section class="show-room entity">
        <div class="detail">
            <nav class="command-panel">
                <h2 class="banner">Eventcategory</h2>
                <a href="/EventCategory/InsertingOne.php" class="tile">Inserting One</a>
            </nav>
            <fieldset></fieldset>
            <div class="feedback">
            </div>
        </div>
        <?php include('ReadingAll.php'); ?>
    </section>
    <footer>
        <p>&copy ModernWays 2020</p>
        <p>Opdracht Programmeren 4</p>
    </footer>
</body>

</html>