<?php
include('include/session.php');
?>
<!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Boekencataloog - werken met cookies en sessies</title>
</head>
<body>
<h1>Boekencataloog</h1>
<h2>Welkom <?php echo isset($userName) ? $userName : 'Je bent niet aangemeld.'; ?></h2>
<p>Je bestellingen:</p>
<p><?php echo isset($shoppingCart) ? $shoppingCart : 'Nog geen boeken besteld.'; ?></p>
<button type="submit" name="shopping-cart" id="empty" value="empty"
        form="catalog-form">Winkelkar leegmaken
</button>
<h3>Bestel een boek</h3>
<form method="post" id="catalog-form" name="catalog-form"
      action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
    <div>
        <label for="book-1">Herman Portocarero, De zwarte handel</label>
        <button id="book-1" name="order" value="Herman Portocarero, De zwarte handel">Bestel</button>
    </div>
    <div>
        <label for="book-2">Aristophanes, Ploutos de god van het geld</label>
        <button id="boek-2" name="order" value="Aristophanes, Ploutos de god van het geld">Bestel</button>
    </div>
    <div>
        <label for="book-3">Steve Prettyman, Learn PHP 8</label>
        <button id="prettyman" name="order" value="Steve Prettyman, Learn PHP 8">Bestel</button>
    </div>
    <div>
        <label for="daudet">Alphonse Daudet, Brieven uit mijn molen</label>
        <button id="daudet" name="order" value="Alphonse Daudet, Brieven uit mijn molen">Bestel</button>
    </div>
</form>
<a href="index.php">Terug naar de index pagina</a>
</body>
</html>