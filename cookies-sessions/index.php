<?php
include('include/session.php');
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Werken met cookies-sessies - logging in</title>
</head>
<body>
<form class="logging-in" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
    <div class="field">
        <label for="user-name">Gebruikersnaam: </label>
        <input id="user-name" name="user-name" class="text" type="text" required/>
    </div>
    <button type="submit" name="submit" id="login" value="login">Aanmelden</button>
</form>
<h1>Welkom <?php echo isset($userName) ? $userName : '';?></h1>

<?php
if (isset($userName)) { ?>
    <form class="logging-out" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">
        <button type="submit" name="submit" id="logout" value="logout">Afmelden</button>
    </form>
    <a href="catalog.php">Ga naar boekencataloog</a>
    <?php
}
?>
</body>
</html>

