<?php
session_start();
?>
<!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Welkom op mijn site!</title>
</head>
<body>
<h1>Welkom</h1>
<h2><?php echo isset($_SESSION['firstName']) ? $_SESSION['firstName'] : 'Je moet je eerst aanmelden'; ?></h2>
<h3><?php echo isset($_SESSION['lastName']) ? $_SESSION['lastName'] : ''; ?></h3>
</body>
</html>


