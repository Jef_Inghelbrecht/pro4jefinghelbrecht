<?php
// deze code dient om te leren werken met cookies en moet je niet in
// een productiewebsite gebruiken!
// kijk als in de request een userName cookie werd meegestuurd:
$userName = isset($_COOKIE['userName']) ? $_COOKIE['userName'] : null;
// kijk als in de request een shopping-cart cookie werd meegestuurd:
$shoppingCart = isset($_COOKIE['shoppingCart']) ? $_COOKIE['shoppingCart'] : null;
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST['shopping-cart']) && $_POST['shopping-cart'] === 'empty') {
        // There is no specific function for deleting a cookie in PHP.
        // However, we recommend you to use the PHP setcookie() function
        // mentioning the expiration date in the past as demonstrated below:
        setcookie("shoppingCart", "", time() - 3600, '/users', '', false, true);
        $shoppingCart = null;

    } elseif (isset($_POST['order'])) {
        // zat er al iets in de shopping-cart cookie?
        if (isset($shoppingCart)) {
            $shoppingCart .= ', ' . $_POST['order'];
        } else {
            $shoppingCart = $_POST['order'];
        }
        setcookie('shoppingCart', $shoppingCart);
    }
}