<?php
//ini_set('session.cookie_domain','modernways.be');
//echo ini_get('session.cookie_domain');
//ini_set('session.cookie_secure',1);
//echo ini_get('session.cookie_secure');
//ini_set('session.cookie_samesite', 'Strict');
//echo ini_get('session.cookie_samesite');
//ini_set('session.cookie_path','c:/xxx');
//echo ini_get('session.cookie_path');
//session_start();
// kijk als in de request een userName cookie werd meegestuurd:
$userName = isset($_COOKIE['userName']) ? $_COOKIE['userName'] : null;
// als op de submit knop werd gedklikt moet je de persoon aanmelden
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if ($_POST['submit'] == 'login') {
        // setcookie: er wordt een cookie meegestuurd naar de client
        // de request die net binnenkwam van de client naar de server
        // bevat geen cookie, die zal slechts de volgende keer
        // dat de request wordt ingediend meegestuurd worden.
        $userName = $_POST['user-name'];
        // setcookie('userName', $userName);
        // setcookie('userName', $userName, 0, '/Testers', '', false, true);
        $cookieParams = array(
            'expires' => 0 /*time() + 5*/,
            'path' => '/',
            'domain' => '',
            'secure' => false,
            'httponly' => true,
            'samesite' => 'Strict');
        setcookie('userName', $userName, $cookieParams);
    } elseif ($_POST['submit'] == 'logout') {
        // There is no specific function for deleting a cookie in PHP.
        // However, we recommend you to use the PHP setcookie() function
        // mentioning the expiration date in the past as demonstrated below:
        setcookie("userName", "", time() - 3600);
        $userName = null;
    }
}
?>