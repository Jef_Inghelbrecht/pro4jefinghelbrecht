<?php
session_name('Programmeren4');
// session_save_path('c:\temp');
// Gets current cookies params
$cookieParams = session_get_cookie_params();
$options = array('lifetime' => $cookieParams["lifetime"],
    'path' => '/',
    'domain' => '',
    'secure' => true,
    'httponly' => true,
    'samesite' => 'Strict');
session_set_cookie_params($options);
session_start();
// phpinfo();
if (session_status() === PHP_SESSION_ACTIVE) {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['submit'])) {
            if ($_POST['submit'] === 'login') {
                $_SESSION['userName'] = $_POST['user-name'];
            }
        } elseif ($_POST['submit'] === 'logout') {
            // Unset all session values
            $_SESSION = array();
            // get session parameters
            $cookieParams = session_get_cookie_params();
            // Delete the actual cookie.
            $options = array('expires' => time() - 42000,
                'path' => $cookieParams['path'],
                'domain' => $cookieParams['domain'],
                'secure' => $cookieParams['secure'],
                'httponly' => $cookieParams['httponly'],
                'samesite' => $cookieParams['samesite']);
            setcookie(session_name(), '', $options);
            // Destroy session
            session_destroy();
        }
    }
}
$userName = isset($_SESSION['userName']) ? $_SESSION['userName'] : null;

