<?php
session_start();
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $_SESSION['firstName'] = $_POST['first-name'];
    $_SESSION['lastName'] = $_POST['last-name'];
}
?>

<!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Leren werken met sessies in PHP</title>
</head>
<body>
<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
    <div>
        <label for="first-name">Voornaam</label>
        <input type="text" name="first-name" id="first-name"
               value="<?php echo isset($_SESSION['firstName']) ? $_SESSION['firstName'] : '' ?>"/>
    </div>
    <div>
        <label for="last-name">Familienaam</label>
        <input type="text" name="last-name" id="last-name"
               value="<?php echo isset($_SESSION['lastName']) ? $_SESSION['lastName'] : '' ?>"/>
    </div>
    <button type="submit" name="submit">Verzenden</button>
</form>
</body>
</html>
