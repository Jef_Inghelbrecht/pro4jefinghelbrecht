<?php

namespace App\Http\Controllers;

use App\EventTopic;
use Illuminate\Http\Request;

class EventTopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $eventTopic = EventTopic::all();
        return view('eventtopic.index', compact('eventTopic'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('eventtopic.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
        EventTopic::create($request->all());
        return redirect()->route('eventtopic.index')

            ->with('success', 'EventTopic created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EventTopic  $eventTopic
     * @return \Illuminate\Http\Response
     */
    public function show(EventTopic $eventTopic)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EventTopic  $eventTopic
     * @return \Illuminate\Http\Response
     */
    public function edit(EventTopic $eventTopic)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EventTopic  $eventTopic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EventTopic $eventTopic)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EventTopic  $eventTopic
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventTopic $eventTopic)
    {
        //
    }
}
