<?php
/*
* JI
* 5/2/2020
* leren werken met variabelen
*/
$firstName = 'Mohamed';
$lastName = 'El Farisi';
$function = 'Teacher';
$age = 45;
$length = 178;
$belgian = TRUE;
var_dump($belgian);
$friends = ['Jan', 'Sarah', 'Mohamed', 'Pascal'];
$rommel = Array('Tekst', 5.4, 4, TRUE, ['Hallo', 'Tot ziens']);
echo "Wat zit er in de rommel array?\n";
?>
<pre>
<?php var_dump($rommel);?>
</pre>


<!DOCTYPE html>
<html lang="nl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/app.css">
    <title>Beginnen met PHP</title>
</head>

<body>
    <a href="werken-met-arrays.php">Werken met array's</a>
    <!-- form>(div>label+input)*6 -->
    <form action="">
        <div><label for="">Voornaam</label><input type="text" value=<?php echo $firstName; ?>></div>
        <div><label for="">Familienaam</label><input type="text" value='<?php echo $lastName; ?>'></div>
        <div><label for="">Functie</label><input type="text" value=<?php echo $function; ?>></div>
        <div><label for="">Leeftijd</label><input type="text" value=<?php echo $age; ?>></div>
        <div><label for="">Lengte</label><input type="text" value=<?php echo $length; ?>></div>
        <div><label for="">Belg?</label><input type="text" value=<?php echo $belgian; ?>></div>
        <div><label for="friends">Vrienden</label>
        <select id="friends">
            <?php
                for ($i = 0; $i <= count($friends) - 1; $i++) {
            ?>
            <option><?php echo $friends[$i];?></option>
            <?php
                }
            ?>
        </select>
        </div>

    </form>
</body>

</html>