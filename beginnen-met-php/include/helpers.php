<?php
/* The delete() function is useful for removing one 
from within a string. 
We simply pass it a string, and a searchstring, 
and the function will take care of the rest for us!
*/
	
// Uses str_replace to remove any unwanted substrings in a string
// Includes the start and end
function delete($searchString, $original) {
	$string = str_replace($searchString, "_PHP_", $original );
	return $string;
}

function makePascalNotation($string) {
    /* retourneert de string waarbij de eerste letter
    van elk woord in hoofdletter wordt gezet en
    alle spaties tussen de woorden wordt verwijderd */  
    return str_replace(' ', '', ucwords($string));
}

function echonl($string, $newline = '<br/>') {
    echo $string . $newline;
}