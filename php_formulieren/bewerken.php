<?php
if (isset($_GET)) {
    // var_dump($_GET);
    foreach ($_GET as $key => $value) {
        $value = htmlspecialchars($value);
        echo "In het veld $key zit de waarde {$value}.";
    }
}

if (isset($_POST)) {
    // var_dump($_POST);
    foreach ($_POST as $key => $value) {
        $value = htmlspecialchars($value);
        echo "In het veld $key zit de waarde {$value}.";
    }
}