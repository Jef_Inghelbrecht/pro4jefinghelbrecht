<?php
// naam van namespece in pascalnotatie
namespace Helpers\Jef;

define('REGEX_ULCHARONLY', '/^[a-zA-Z ]*$/');

class Validate
{
    public static function regex($value, $regex)
    {
        if (preg_match($regex, $value)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public static function filter($value, $filter, $errorMessage = "Fout")
    {
        if (filter_var($value, $filter)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}
