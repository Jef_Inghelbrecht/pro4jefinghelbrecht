<?php

$book = array(
    'Title' => 'L\'être et le néant',
    'Author' => 'Jean-Paul Sartre',
    'Detail' => array(
        'Publisher' => 'Gallimard',
        'Date' => '1943'
    ));



function echoArrayRecursive($array){
    foreach($array as $key => $value){
        //If $value is an array.
        if(is_array($value)){
            //We need to loop through it.
            echoArrayRecursive($value);
        } else{
            //It is not an array, so print it out.
            echo $value, '<br>';
        }
    }
}

function echoArrayRecursiveIndent($array, $level = 1){
    foreach($array as $key => $value){
        //If $value is an array.
        if(is_array($value)){
            //We need to loop through it.
            echoArrayRecursiveIndent($value, ++$level);
        } else{
            //It is not an array, so print it out.
            echo str_repeat('-', $level), $value, '<br>';
        }
    }
}

function echoArrayRecursiveWithKey($array){
    foreach($array as $key => $value){
        //If $value is an array.
        if(is_array($value)){
            //We need to loop through it.
            echoArrayRecursiveWithKey($value);
        } else{
            //It is not an array, so print it out.
            echo "$key: $value <br>";
        }
    }
}

// echoArrayRecursive($book);
$sampleNumbers = array(10, 5, 3, 8, 20, 7, 4, 1, 3, 12, 14, 6, 4, 1, 1);
echoArrayRecursive($sampleNumbers);
// echoArrayRecursiveIndent($book);
echoArrayRecursiveWithKey($book);