<?php

function numbersToAbacus($numbers)
{
    $maxNumber = max($numbers);
    // $row = str_repeat('_', $maxNumber);
    $abacus = array();
    foreach ($numbers as $number) {
        $abacus[] = str_repeat('*', $number) . str_repeat('_', $maxNumber - $number);
    }
    return $abacus;
}

function rotateAbacus($abacus)
{
    $rotatedAbacus = array_fill(0, strlen($abacus[0]), str_repeat('=', count($abacus) - 1,));
    for ($i = 0; $i < count($abacus); $i++) {
        for ($j = 0; $j < strlen($abacus[$i]); $j++) {
            $rotatedAbacus[$j][$i] = $abacus[$i][$j];
        }
    }
    // var_dump($rotatedAbacus);
    return $rotatedAbacus;
}

function echoAbacus($abacus)
{
    foreach ($abacus as $row) {
        echo "$row<br/>";
    }
}

function gravitateAbacus($abacus)
{
    $maxNumber = strlen($abacus[0]);
    for ($i = 0; $i < count($abacus); $i++) {
        $number = substr_count($abacus[$i], '*');
        $abacus[$i] = str_repeat('*', $number) . str_repeat('_', $maxNumber - $number);
    }
    return $abacus;
}

function abacusToNumbers($abacus)
{
    $numbers = array();
    for ($i = count($abacus) - 1; $i >= 0; $i--) {
        $number = substr_count($abacus[$i], '*');
        $numbers[] = $number;
    }
    return $numbers;
}

function echoArray($numbers)
{
    foreach ($numbers as $number) {
        echo "$number<br/>";
    }
}

$sampleNumbers = array(10, 5, 3, 8, 20, 7, 4, 1, 3, 12, 14, 6, 4, 1, 1);
$sampleAbacus = numbersToAbacus($sampleNumbers);
$rotatedAbacus = rotateAbacus($sampleAbacus);
$gravitatedAbacus = gravitateAbacus($rotatedAbacus);
$rotatedBackAbacus = rotateAbacus($gravitatedAbacus);
$sortedNumbers = abacusToNumbers($rotatedBackAbacus);

?>

<!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bead sort algoritm</title>
    <style>
        body {
            display: flex;
            justify-content: space-around;
        }
    </style>
</head>
<body>
<p>
    <?php echoArray($sampleNumbers); ?>
</p>
<p>
    <?php echoAbacus($sampleAbacus); ?>
</p>
<p>
    <?php echoAbacus($rotatedAbacus); ?>
</p>

<p>
    <?php echoAbacus($gravitatedAbacus); ?>
</p>
<p>
    <?php echoAbacus($rotatedBackAbacus); ?>
</p>
<p>
    <?php echoArray($sortedNumbers); ?>
</p>

</body>
</html>
