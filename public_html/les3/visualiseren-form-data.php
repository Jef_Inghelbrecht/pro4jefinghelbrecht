<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["firstname"])) {
        $firstnameErr = "Voornaam is verplicht";
    } else {
        $firstname = test_input($_POST["firstname"]);
    }

    if (empty($_POST["lastname"])) {
        $lastnameErr = "Familienaam is verplicht";
    } else {
        $lastname = test_input($_POST["lastname"]);
    }

    if (empty($_POST["mail"])) {
        $mailErr = "Email is verplicht";
    } else {
        $mail = test_input($_POST["mail"]);
    }

    if (empty($_POST["mail"])) {
        $mailErr = "Email is verplicht";
    } else {
        $mail = test_input($_POST["mail"]);
        // check if e-mail address is well-formed
        if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
            $mailErr = "Ongeldig email formaat";
        }
    }

    // Validate password strength
    $password1 = test_input($_POST["password1"]);
    $uppercase = preg_match('@[A-Z]@', $password1);
    $lowercase = preg_match('@[a-z]@', $password1);
    $number = preg_match('@[0-9]@', $password1);
    $specialChars = preg_match('@[^\w]@', $password1);


    if (empty($_POST["password1"])) {
        $password1Err = "Paswoord is verplicht";
    } else if (!$uppercase || !$lowercase || !$number || !$specialChars || strlen($password1) < 8) { //
        $passwordNotMatchErr = 'Paswoord moet minstens 8 tekens bevateene en moet een hoofletter, een nummer en een speciaal teken bevatten.';
    } else {
        $password1 = test_input($_POST["password1"]);
    }


    if (empty($_POST["password2"])) {
        $password2Err = "Paswoord is verplicht";
    } else {
        $password2 = test_input($_POST["password2"]);
    }

    if ($_POST["password1"] != $_POST["password2"]) {
        $passwordNotMatchErr = "Paswoorden zijn niet identiek";
    }

    if (empty($_POST["address1"])) {
        $address1Err = "Adres is verplicht";
    } else {
        $address1 = test_input($_POST["address1"]);
    }

    if (empty($_POST["address2"])) {
        $address2 = "";
    } else {
        $address2 = test_input($_POST["address2"]);
    }

    if (empty($_POST["postalcode"])) {
        $postalcodeErr = "Postcode is verplicht";
    } else {
        $postalcode = test_input($_POST["postalcode"]);
    }


    if (empty($_POST["city"])) {
        $cityErr = "Adres is verplicht";
    } else {
        $city = test_input($_POST["city"]);
    }

    if (empty($_POST["gsm"])) {
        $gsm = "";
    } else {
        $gsm = test_input($_POST["gsm"]);
    }

    if (empty($_POST["birthday"])) {
        $birthdayErr = "Geboortedatum is verplicht";
    } else {
        $birthday = test_input($_POST["birthday"]);
    }

    if (empty($_POST["satisfied"])) {
        $satisfied = "";
    } else {
        $satisfied = test_input($_POST["satisfied"]);
    }

    if (empty($_POST["sex"])) {
        $sexErr = "Geslacht is verplicht";
    } else {
        $sex = $_POST["sex"];
    }

    if ($_POST['countryCode'] == "") {
        $country = "";
    } else {
        $country = test_input($_POST["countryCode"]);
    }

    if ($_POST['faculty'] == "") {
        $faculty = "";
    } else {
        $faculty = test_input($_POST["faculty"]);
    }

//    if ($_POST['course'] == "") {
    if (empty($_POST['course'])) { //check if array is empty
        $course = "";
    } else {
        $course = $_POST["course"];
    }

//     fomrulier laden wanneer alle velden correct zijn ingevuld
//    if ($lastnameErr == "" && $firstnameErr == "") {
//        header('location: visualiseren-form-data.php');
//        exit();
//    }
}


function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>
<!DOCTYPE html>
<html>

<head>
    <title>PHP formulier werken met PHP</title>
    <meta charset="utf-8" />
    <meta name="application-name" content="PHP form">
    <meta name="description" content="Formulieren html en PHP">
    <meta name="keywords" content="HTML, CSS, PHP">
    <meta name="author" content="Tim Janssens">
</head>

<body>
</body>
<p>Visuele voorstelling van een associatieve array of dictionary in PHP</p>
<p><?php echo var_dump($_POST); ?></p>
<p><?php echo print_r($_POST); ?></p>

<article>
    <ol>
        <li>Naam:
            <?php echo isset($_POST['firstname']) ? $_POST['firstname'] : 'Naam niet opgegeven!'; ?>
        </li>
        <li>Familienaam:
            <?php echo isset($_POST['lastname']) ? $_POST['lastname'] : 'Naam niet opgegeven!'; ?>
        </li>
        <li>Email: :
            <?php echo isset($_POST['mail']) ? $_POST['mail'] : 'Email niet opgegeven!'; ?>
        </li>
        <li>Paswoord:
            <?php echo isset($_POST['password1']) ? $_POST['password1'] : 'Paswoord niet opgegeven!'; ?>
        </li>
        <li>Paswoord herhaling:
            <?php echo isset($_POST['password2']) ? $_POST['password2'] : 'Passwoord herhaling niet opgegeven!'; ?>
        </li>
        <li>Adres:
            <?php echo isset($_POST['address1']) ? $_POST['address1'] : 'Adres niet opgegeven!'; ?>
        </li>
        <li>Adres 2:
            <?php echo isset($_POST['address2']) ? $_POST['address2'] : 'Adres niet opgegeven!'; ?>
        </li>
        <li>Postcode:
            <?php echo isset($_POST['postalcode']) ? $_POST['postalcode'] : 'Postcode niet opgegeven!'; ?>
        </li>
        <li>Stad:
            <?php echo isset($_POST['city']) ? $_POST['city'] : 'Stad niet opgegeven!'; ?>
        </li>
        <li>Land:
            <?php echo isset($_POST['countrycode']) ? $_POST['countrycode'] : 'Landcode niet opgegeven!'; ?>
        </li>
        <li>Gsm-nummer:
            <?php echo isset($_POST['gsm']) ? $_POST['gsm'] : 'Gsm niet opgegeven!'; ?>
        </li>
        <li>Geboortedatum:
            <?php echo isset($_POST['birthday']) ? $_POST['birthday'] : 'Geboortedatum niet opgegeven!'; ?>
        </li>
        <li>Tevredenheid:
            <?php echo isset($_POST['satisfied']) ? $_POST['satisfied'] : 'Tevredenheid niet opgegeven!'; ?>
        </li>
        <li>Geslacht:
            <?php echo isset($_POST['sex']) ? $_POST['sex'] : 'Geslacht niet opgegeven!'; ?>
        </li>
        <li>Richting:
            <?php echo isset($_POST['faculty']) ? $_POST['faculty'] : 'Richting niet opgegeven!'; ?>
        </li>
        <li>Cursussen:
            <?php
            $courses = '';
            if(!empty($_POST['course'])) {
                foreach ($_POST['course'] as $item) {
                    $courses = $courses . " $item";
                }
            }
            echo isset($_POST['course']) ? $courses : 'Cursussen niet opgegeven!'; ?>

        </li>
    </ol>
</article>
</html>