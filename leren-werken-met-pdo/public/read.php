<?php
    // alleen uit te voeren als er op de submit knop is gedrukt
    if (isset($_POST['submit'])) {
        include('../config.php');
        include('../common.php');
        $location = escape($_POST['Location']);
        $statement = false;
        try {
            $sql = 'SELECT * FROM Users WHERE Location = :Location';
             // echo $sql;
            // return;
            $connection = new \PDO($host, $username, $password, $options);
            $statement = $connection->prepare($sql);
            $statement->bindParam(':Location', $location);
            $statement->execute();
            $result = $statement->fetchAll();

        } catch (\PDOException $exception) {
            echo $sql . '<br/>' . $exception->getMessage();
        }
    }

    include('template/header.php');
?>


<!-- form>(div>label+input[id][name])*5  -->
<form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">
    <div><label for="Location">Plaats</label><input type="text" id="Location" name="Location"></div>
    <button type="submit" name="submit" value="create-person">Zoeken</button>
</form>

<table>
    <thead>
        <tr>
            <th>#</th>
            <th>Voornaam</th>
            <th>Familienaam</th>
            <th>Email</th>
            <th>Leeftijd</th>
            <th>Plaats</th>
            <th>Date</th>
        </tr>
    </thead>
    <tbody>
<?php
    if (isset($_POST['submit'])) {
        if ($result && $statement->rowCount() > 0) {
            foreach ($result as $row) {
?>
            <tr>
                <td><?php echo $row['Id'];?></td>
                <td><?php echo $row['FirstName'];?></td>
                <td><?php echo $row['LastName'];?></td>
                <td><?php echo $row['Email'];?></td>
                <td><?php echo $row['Age'];?></td>
                <td><?php echo $row['Location'];?></td>
                <td><?php echo $row['Date'];?></td>
            </tr>        
<?php
            }
        }
    }

?>
    </tbody>
</table>

<?php
    include('template/footer.php');
?>