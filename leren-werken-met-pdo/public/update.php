<?php
    include('../config.php');
    include('../common.php');
    $statement = false;
              
    try{
        // de : voor de variabelen wil zeggen dat dat een variabele is
        $sql = 'SELECT * FROM Users';
        
        //dit zijn de variabelen uit de config file.
        $connection = new \PDO($host, $username, $password, $options);
      
        $statement = $connection->prepare($sql);
        $statement->execute();
        $result = $statement -> fetchAll();
        
    }
    catch (\PDOException $exception){
        echo $sql .'<br/>' . $exception->getMessage();
    }
    
    include('template/header.php');
    
?>

<table>
<thead>
    <tr>
        <th>#</th>
        <th>Voornaam</th>
        <th>Familienaam</th>
        <th>Email</th>
        <th>Leeftijd</th>
        <th>Plaats</th>
        <th>Date</th>
        <th>Edit</th>
    </tr>
</thead>
<tbody>
<?php

if ($result && $statement->rowCount() > 0) {
    foreach ($result as $row) {
?>
    <tr>
        <td><?php echo escape($row['Id']);?></td>
        <td><?php echo escape($row['FirstName']);?></td>
        <td><?php echo escape($row['LastName']);?></td>
        <td><?php echo escape($row['Email']);?></td>
        <td><?php echo escape($row['Age']);?></td>
        <td><?php echo escape($row['Location']);?></td>
        <td><?php echo escape($row['Date']);?></td>
        <td><a href="update-single.php?Id=<?php echo escape($row['Id']);?>"></a></td>
    </tr>        
<?php
    }
}


?>
</tbody>
</table>

<?php
    include('template/footer.php');
?>