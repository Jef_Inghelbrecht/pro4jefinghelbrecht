<?php
    include('templates/header.php');
    //alleen uit te voeren als er op de submit knop is gedrukt.
    if(isset($_POST['submit'])){
        include('../config.php');
        include('../common.php');
        
         $newUser = array(
             //de escape methode staat in de common.php file.
            'FirstName' => escape($_POST['FirstName']),
            'LastName' => escape($_POST['LastName']),
            'Email' => escape($_POST['Email']),
            'Age' => escape($_POST['Age']),
            'Location' => escape($_POST['Location']),
            'Date' => escape($_POST['Date'])
                );
                
        try{
            // de : voor de variabelen wil zeggen dat dat een variabele is
            $sql = 'INSERT INTO Users (FirstName, LastName, Email, Age, Location, Date) VALUES(:FirstName, :LastName, :Email, :Age, :Location, :Date)';
                    //placeholders --> PascalNotatie
            
            //dit zijn de variabelen uit de config file.
            $connection = new \PDO($host, $username, $password, $options);
            
            $statement = $connection->prepare($sql);
            $statement -> bindParam(':FirstName', $newUser['FirstName']);
            $statement -> bindParam(':LastName', $newUser['LastName']);
            $statement -> bindParam(':Email', $newUser['Email']);
            $statement -> bindParam(':Age', $newUser['Age']);
            $statement -> bindParam(':Location', $newUser['Location']);
            $statement -> bindParam(':Date', $newUser[':Date']);
            $statement -> execute();
            
        }
        catch (\PDOException $exception){
            echo $sql .'<br/>' . $exception->getMessage();
        }
    }
?>

<div id="feedback">
    <?php
    if(isset($_POST['submit']) && $statement){
        echo "{$newUser['FirstName']} {$newUser['LastName']} is toegevoegd.";
    }
    ?>
</div>
<h2>Add a user</h2>
<form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">
    
    <div>
        <label for="FirstName">First Name</label>
        <input type="text" name="FirstName" id="FirstName">
    </div>
    
    <div>
        <label for="LastName">Last name</label>
        <input type="text" name="LastName" id="LastName">
    </div>
    
    <div>
        <label for="Email">Email address</label>
        <input type="text" name="Email" id="Email">
    </div>
    
    <div>
        <label for="Age">age</label>
        <input type="text" name="Age" id="Age">
    </div>
    
    <div>
        <label for="Location">Location</label>
        <input type="text" name="Location" id="Location">
    </div>
    
     <div>
        <label for="Date">Date</label>
        <input type="date" name="Date" id="Date">
    </div>
    
    <input type="submit" name="submit" value="Submit"/>
    
</form>
<php
    include('../templates/footer.php');
?>