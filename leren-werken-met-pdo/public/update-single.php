<?php
include('template/header.php');
if(isset($_GET['Id'])){
    include('../config.php');
    include('../common.php');
    $Id = escape($_GET['Id']);
    $statement = false;
    try {
        $sql = 'SELECT * FROM Users WHERE Id = :Id';
         // echo $sql;
        // return;
        $connection = new \PDO($host, $username, $password, $options);
        $statement = $connection->prepare($sql);
        $statement->bindParam(':Id', $Id);
        $statement->execute();
        $result = $statement->fetch(\PDO::FETCH_ASSOC);
        //$result = wat men retourneert moet een associatieve array zijn.

    } catch (\PDOException $exception) {
        echo $sql . '<br/>' . $exception->getMessage();
    }
}

if(isset($_POST['submit'])){
    include('../config.php');
    include('../common.php');
    $user = array(
         //de escape methode staat in de common.php file.
        'Id' => escape($_POST['Id']),
        'FirstName' => escape($_POST['FirstName']),
        'LastName' => escape($_POST['LastName']),
        'Email' => escape($_POST['Email']),
        'Age' => escape($_POST['Age']),
        'Location' => escape($_POST['Location']),
        'Date' => escape($_POST['Date'])
    );
    
  try{
        // de : voor de variabelen wil zeggen dat dat een variabele is
        $sql = 'UPDATE Users SET FirstName = :FirstName, LastName = :LastName, Email = :Email, Age = :Age, Location = :Location, Date = :Date
                    WHERE Id = :Id';
                //placeholders --> PascalNotatie
        
        //dit zijn de variabelen uit de config file.
        $connection = new \PDO($host, $username, $password, $options);
        
        $statement = $connection->prepare($sql);
        $statement -> bindParam(':Id', $user['Id']);
        $statement -> bindParam(':FirstName', $user['FirstName']);
        $statement -> bindParam(':LastName', $user['LastName']);
        $statement -> bindParam(':Email', $user['Email']);
        $statement -> bindParam(':Age', $user['Age']);
        $statement -> bindParam(':Location', $user['Location']);
        $statement -> bindParam(':Date', $user['Date']);
        $statement -> execute();
        
    }
    catch (\PDOException $exception){
        echo $sql .'<br/>' . $exception->getMessage();
    }
}

?>

<div id="feedback">
    <?php
    if(isset($_POST['submit']) && $statement){
        echo "{$user['FirstName']} {$user['LastName']} is gewijzigd.";
    }
    ?>
</div>
<h2>Gebruiker wijzigen</h2>
<form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">
    
<input type="hidden" name="Id" id="Id"
        value="<?php echo escape($result['Id']); ?>">
<div>
    <label for="FirstName">First Name</label>
    <input type="text" name="FirstName" id="FirstName"
        value="<?php echo escape($result['FirstName']); ?>">
</div>

<div>
    <label for="LastName">Last name</label>
    <input type="text" name="LastName" id="LastName" 
        value="<?php echo escape($result['LastName']); ?>">
</div>

<div>
    <label for="Email">Email address</label>
    <input type="text" name="Email" id="Email"
        value="<?php echo escape($result['Email']); ?>">
</div>

<div>
    <label for="Age">age</label>
    <input type="text" name="Age" id="Age"
        value="<?php echo escape($result['Age']); ?>">
</div>

<div>
    <label for="Location">Location</label>
    <input type="text" name="Location" id="Location"
        value="<?php echo escape($result['Location']); ?>">
</div>

 <div>
    <label for="Date">Date</label>
    <input type="datetime" name="Date" id="Date"
        value="<?php echo escape($result['Date']); ?>">
</div>

<input type="submit" name="submit" value="Update"/>

</form>
<php
include('template/footer.php');
?>
